//var host = "https://backendv2.crawfordaffinitycolombia.com";
var host = "http://ec76270e.ngrok.io/crawford/Crawford/public";
//var host = "http://192.168.1.18:8080/push_Crawford/crawford/Crawford/public";
// var host = "http://192.168.77.87";

angular.module('app.controllers', [])

    .controller('menuCtrl', ['$scope', '$stateParams', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $state) {

            $scope.cerrarsesion = function () {
                localStorage.setItem("CRAWFORD_cliente", '');
                localStorage.setItem("CRAWFORD_nombre", '');
                localStorage.setItem("CRAWFORD_email", '');
                localStorage.setItem("CRAWFORD_apellido", '');
                localStorage.setItem("CRAWFORD_cedula", '');
                localStorage.setItem("CRAWFORD_fechanacimiento", '');
                localStorage.setItem("CRAWFORD_celular", '');
                localStorage.setItem("CRAWFORD_fechaCC", '');
                localStorage.setItem("CRAWFORD_marca", '');
                localStorage.setItem("CRAWFORD_tipo_seguro", '');
                localStorage.setItem("CRAWFORD_aseguradora", '');
                //ir al inicio
                $state.go('inicioDeSesiN');
            };

        }])

    
    .controller('menu_1Ctrl', ['$scope', '$stateParams', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $state) {

            $scope.cerrarsesion = function () {
                localStorage.setItem("CRAWFORD_cliente", '');
                localStorage.setItem("CRAWFORD_nombre", '');
                localStorage.setItem("CRAWFORD_email", '');
                localStorage.setItem("CRAWFORD_apellido", '');
                localStorage.setItem("CRAWFORD_cedula", '');
                localStorage.setItem("CRAWFORD_fechanacimiento", '');
                localStorage.setItem("CRAWFORD_celular", '');
                localStorage.setItem("CRAWFORD_fechaCC", '');
                localStorage.setItem("CRAWFORD_marca", '');
                localStorage.setItem("CRAWFORD_tipo_seguro", '');
                localStorage.setItem("CRAWFORD_aseguradora", '');
                //ir al inicio
                $state.go('inicioDeSesiN');
            };

        }])

    .controller('inicioDeSesiNCtrl', ['$scope', '$stateParams', '$http', '$state', '$cordovaDialogs', '$ionicLoading', '$rootScope',
        function ($scope, $stateParams, $http, $state, $cordovaDialogs, $ionicLoading, $rootScope) {

            $scope.data = {
                'email': "",
                'password': "",
                'marca': "1",
                'tipo_seguro': "3",
                'aseguradora': "1",
                'device_token': localStorage.getItem('device_token') ? localStorage.getItem('device_token') : ''
            };

            $scope.login = function () {
                // Setup the loader
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                console.log($scope.data); //datos a enviar
                if (this.validate() !== false) {
                    $http({
                        url: host + '/clientes/webapp/login',
                        method: "POST",
                        contentType: "text/xml",
                        dataType: "text",
                        data: $scope.data
                    }).then(function (result) {
                        console.log(result);
                        if (result.data.Status == 'false') {
                            $ionicLoading.hide();
                            alert(result.data.Message);
                        }
                        else {
                            $ionicLoading.hide();
                            $scope.save(result.data.body[0]);
                            $state.go('menu.bienvenid');
                        }
                    }, function (err) {
                        console.log(err);
                        $ionicLoading.hide();
                    });
                }
            };

            $scope.save = function (cliente) {
                localStorage.setItem("CRAWFORD_cliente", cliente.id);
                localStorage.setItem("CRAWFORD_nombre", cliente.nombre);
                localStorage.setItem("CRAWFORD_email", cliente.email);
                localStorage.setItem("CRAWFORD_apellido", cliente.lastname);
                localStorage.setItem("CRAWFORD_cedula", cliente.cedula);
                localStorage.setItem("CRAWFORD_fechanacimiento", cliente.fechanacimiento);
                localStorage.setItem("CRAWFORD_celular", cliente.celular);
                localStorage.setItem("CRAWFORD_fechaCC", cliente.fechaCC);
                localStorage.setItem("CRAWFORD_marca", $scope.data.marca);
                localStorage.setItem("CRAWFORD_tipo_seguro", $scope.data.tipo_seguro);
                localStorage.setItem("CRAWFORD_aseguradora", $scope.data.aseguradora);
            };

            $scope.validate = function () {
                if ($scope.data.email === '' || $scope.data.password === '') {
                    alert('Presentas alguna falla en los datos verifica');
                    $ionicLoading.hide();
                    return false;
                }
            };

            $scope.go_olvido = function(){
                $state.go('olvido');
            };

            //PUSH NOTIFICATION


        }])

.controller('olvidoCtrl', ['$scope', '$stateParams', '$http', '$state', '$cordovaDialogs', '$ionicLoading', '$rootScope',
function ($scope, $stateParams, $http, $state, $cordovaDialogs, $ionicLoading, $rootScope) {

    $scope.form = {
        'email' : '',
        'marca' : localStorage.getItem('CRAWFORD_marca')
    };

    $scope.send = function(){
        //verificar si el chat esta abierto o no 
        $http({
            url: host + '/clientes/webapp/olvido',
            method: "POST",
            data: $scope.form
        }).then(function (result) {
            console.log(result);
            $state.go('inicioDeSesiN');
        }, function (err) {
            console.log(err);
        }); 
    };


}])

    .controller('bienvenidCtrl', ['$scope', '$stateParams', '$http', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state) {


            $scope.bienvenido = {
                'id': localStorage.getItem("CRAWFORD_cliente"),
                'nombre': localStorage.getItem("CRAWFORD_nombre"),
                'apellido': localStorage.getItem("CRAWFORD_apellido"),
                'email': localStorage.getItem("CRAWFORD_email"),
                'cedula': localStorage.getItem("CRAWFORD_cedula")
            };

            $scope.go_misReclamos = function () {
                $state.go('menu.misReclamos', {
                    CRAWFORD_cliente: $scope.bienvenido.id
                });

            };

            $scope.go_misPolizas = function () {
                alert("mis polizas");
                //$state.go('menu.misPolizas');
            };

            $scope.go_ayuda = function () {
                //alert("ayuda");
                //$state.go('menu.ayuda');
            };


        }])

    .controller('terminosCtrl', ['$scope', '$stateParams', '$http', '$state', '$ionicPlatform', '$ionicHistory',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state, $ionicPlatform, $ionicHistory) {

            $scope.aceptar = function(){
                localStorage.setItem("CRAWFORD_terminos", true);
                $ionicHistory.goBack();
            };            
            

        }])

    .controller('ayudaCtrl', ['$scope', '$stateParams', '$http', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state) {

            $scope.openChat = function(){
                //verificar si el chat esta abierto o no 
                $http({
                    url: host + '/utilchats/search',
                    method: "GET"
                }).then(function (result) {
                    console.log(result);
                    if(result.data.body.estado == "HABILITADO"){
                        $state.go('menu.chat');
                    }
                    else{
                        alert(result.data.body.mensajecierre);
                    }
                }, function (err) {
                    console.log(err);
                });                
            }

        }])

    .controller('registrarmeCtrl', ['$scope', '$stateParams', '$http', '$state', '$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state, $ionicLoading) {

            $scope.registro = {
                'tokenAPI': 'NEWTOKEN',
                'clientid': 'NEWCLIENT',
                'marca': "1",
                'tipo_seguro': "3",
                'aseguradora': "1",
                'nombre': "",
                'lastname': "",
                'fechanacimiento': "",
                'email': "",
                'celular': "",
                'cedula': "",
                'fechaCC': "",
                'autoriza': localStorage.getItem("CRAWFORD_terminos"),
                'password': "",
                'password2': ""                
            };

            $scope.update_register = function () {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                console.log($scope.registro);
                if (this.validate() !== false) {
                    $http({
                        url: host + '/clientes/create',
                        method: "POST",
                        contentType: "text/xml",
                        dataType: "text",
                        data: $scope.registro,
                    }).then(function (result) {
                        console.log(result);
                        if (result.data.Status == 'false') {
                            alert(result.data.Message);
                            $ionicLoading.hide();
                        }
                        else {
                            $scope.save(result.data.body);
                            $ionicLoading.hide();
                            $state.go('menu.bienvenid');
                        }
                    }, function (err) {
                        console.log(err);
                        $ionicLoading.hide();
                    });
                }
            };

            $scope.validate = function () {    
                var respuesta = false;            
                angular.forEach($scope.registro,function(value,index){
                    console.log("VALIDANDO ... " + index);
                    if(value === "" || value == null){
                        console.log("    ERROR en " + index);
                        $scope.error =  "No has cumplido con la información requerida verifica "+ index +" y los campos restantes";
                        $scope.class = "error";
                        $ionicLoading.hide();
                        respuesta = false;
                        exit();
                    }
                    else{
                        //console.log("OK");
                        $scope.error =  "";
                        $scope.class = "success";
                        respuesta = true;
                    }
                });

                if ($scope.registro.password != $scope.registro.password2  ) {
                    $scope.error= "Contraseñas no coinciden";
                    $scope.class = "error";
                    $ionicLoading.hide();
                    respuesta = false;
                }
                if(localStorage.getItem('CRAWFORD_terminos') === ''){
                    alert('Acepta terminos y condiciones');
                    respuesta = false;
                }
                return respuesta;
            };

            $scope.save = function (cliente) {
                localStorage.setItem("CRAWFORD_cliente", cliente.id);
                localStorage.setItem("CRAWFORD_nombre", cliente.nombre);
                localStorage.setItem("CRAWFORD_email", cliente.email);
                localStorage.setItem("CRAWFORD_apellido", cliente.lastname);
                localStorage.setItem("CRAWFORD_cedula", cliente.cedula);
                localStorage.setItem("CRAWFORD_fechanacimiento", cliente.fechanacimiento);
                localStorage.setItem("CRAWFORD_celular", cliente.celular);
                localStorage.setItem("CRAWFORD_fechaCC", cliente.fechaCC);
                localStorage.setItem("CRAWFORD_marca", $scope.registro.marca);
                localStorage.setItem("CRAWFORD_tipo_seguro", $scope.registro.tipo_seguro);
                localStorage.setItem("CRAWFORD_aseguradora", $scope.registro.aseguradora);
            };

            $scope.terminos = function(){
                $state.go('terminos');
            };

        }])

    .controller('miPerfilCtrl', ['$scope', '$stateParams', '$http', '$state', '$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state, $ionicLoading) {

            $scope.form = {
                'tokenAPI': 'NEWTOKEN',
                'clientid': 'NEWCLIENT',
                'marca': "1",
                'tipo_seguro': "3",
                'aseguradora': "1",
                'nombre': localStorage.getItem("CRAWFORD_nombre"),
                'lastname': localStorage.getItem("CRAWFORD_apellido"),
                'fechanacimiento': localStorage.getItem("CRAWFORD_fechanacimiento"),
                'email': localStorage.getItem("CRAWFORD_email"),
                'celular': localStorage.getItem("CRAWFORD_celular"),
                'cedula': localStorage.getItem("CRAWFORD_cedula"),
                'fechaCC': localStorage.getItem("CRAWFORD_fechaCC"),
                'autoriza': localStorage.getItem("CRAWFORD_terminos"),
                'password':"",
                'id': localStorage.getItem("CRAWFORD_cliente")
            };

            $scope.update = function () {
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                console.log($scope.form);
                if (this.validate() !== false) {
                    $http({
                        url: host + '/clientes/update',
                        method: "POST",
                        contentType: "text/xml",
                        dataType: "text",
                        data: $scope.form,
                    }).then(function (result) {
                        console.log(result);
                        if (result.data.Status == 'false') {
                            alert(result.data.Message);
                            $ionicLoading.hide();
                        }
                        else {
                            $scope.save(result.data.body);
                            $ionicLoading.hide();
                            alert('Actualizado con exito');
                            $state.go('menu.bienvenid');
                        }
                    }, function (err) {
                        console.log(err);
                        $ionicLoading.hide();
                    });
                }
            };

            $scope.validate = function () {    
                var respuesta = false;            
                angular.forEach($scope.form,function(value,index){
                    console.log("VALIDANDO ... " + index);
                    if(value === "" || value == null){
                        console.log("    ERROR en " + index);
                        $scope.error =  "No has cumplido con la información requerida verifica "+ index +" y los campos restantes";
                        $scope.class = "error";
                        $ionicLoading.hide();
                        respuesta = false;
                        exit();
                    }
                    else{
                        //console.log("OK");
                        $scope.error =  "";
                        $scope.class = "success";
                        respuesta = true;
                    }
                });

                if(localStorage.getItem('CRAWFORD_terminos') === ''){
                    alert('Acepta terminos y condiciones');
                    respuesta = false;
                }
                return respuesta;
            };

            $scope.save = function (cliente) {
                localStorage.setItem("CRAWFORD_cliente", cliente.id);
                localStorage.setItem("CRAWFORD_nombre", cliente.nombre);
                localStorage.setItem("CRAWFORD_email", cliente.email);
                localStorage.setItem("CRAWFORD_apellido", cliente.lastname);
                localStorage.setItem("CRAWFORD_cedula", cliente.cedula);
                localStorage.setItem("CRAWFORD_fechanacimiento", cliente.fechanacimiento);
                localStorage.setItem("CRAWFORD_celular", cliente.celular);
                localStorage.setItem("CRAWFORD_fechaCC", cliente.fechaCC);
                localStorage.setItem("CRAWFORD_marca", cliente.marca);
                localStorage.setItem("CRAWFORD_tipo_seguro", cliente.tipo_seguro);
                localStorage.setItem("CRAWFORD_aseguradora", cliente.aseguradora);
            };


        }])

    .controller('misReclamosCtrl', ['$scope', '$stateParams', '$http', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state) {

            //trar los datos de id
            $scope.misReclamos_data = $stateParams;

            //intervalos de 3 segundos y refresca los Datos
            this.task = setInterval(function() {
                $scope.refreshData();
            }, 8000);

            $scope.refreshData = function(){
                //traer todos los reclamos
                    $http({
                        url: host + '/reclamos/cliente/' + localStorage.getItem("CRAWFORD_cliente"),
                        method: "GET"
                    }).then(function (result) {
                        console.log(result);
                        $scope.casos = result.data.body;
                    }, function (err) {
                        console.log(err);
                    });
            };
            

            $scope.go_checklist = function (id_reclamo, tipo_poliza) {
                $state.go('checklist', {
                    "id_reclamo": id_reclamo,
                    "tipo_poliza": tipo_poliza
                });
            };

            $scope.go_block = function(id_reclamo, tipo_poliza){
                $state.go('menu.caso1402', {
                    "id_reclamo": id_reclamo,
                    "tipo_poliza": tipo_poliza,
                    "ayuda": "Caso en estudio"
                });
            };

        }])

    .controller('nuevoReclamo13Ctrl', ['$scope', '$stateParams', '$http', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state) {

            $scope.form = {
                'cedula': localStorage.getItem("CRAWFORD_cedula"),
                'nombreasegurado': localStorage.getItem("CRAWFORD_nombre"),
                'apellidoasegurado': localStorage.getItem("CRAWFORD_apellido"),
                'fechanacimientoasse': new Date(localStorage.getItem("CRAWFORD_fechanacimiento")),
                'email': localStorage.getItem("CRAWFORD_email"),

                'marca': localStorage.getItem("CRAWFORD_marca"),
                'tipo_seguro': localStorage.getItem("CRAWFORD_tipo_seguro"),
                'aseguradora': localStorage.getItem("CRAWFORD_aseguradora"),

                'ciudadrecidencia': '',
                'departrecidencia': '',
                'telefonofijo': '',
                'celular': localStorage.getItem("CRAWFORD_celular"),
                'direccion': '',
                'telefonolaboral': ''
            };

            //traer todos los departamentos
            $http({
                url: host + '/ciudades/all',
                method: "GET"
            }).then(function (result) {
                console.log(result);
                $scope.departamentos = result.data.body;
            }, function (err) {
                console.log(err);
            });

            //cargar departamentos

            $scope.cargarCiudades = function(){
                id = document.getElementById('departamento').value;
                console.log('id de departamento',id);
                
                $http({
                    url: host + '/ciudades/show_ciudades/'+ id,
                    method: 'GET'
                }).then(function (response){
                    console.log(response);
                    $scope.ciudades = response.data.body;
                    
                }, function (e){
                    console.log(e);
                    
                })
            }

            $scope.go_pass2 = function () {
                if (this.validate() !== false) {
                    $state.go('nuevoReclamo23', $scope.form);
                }
            };

            $scope.validate = function () {    
                var respuesta = false;            
                angular.forEach($scope.form,function(value,index){
                    console.log("VALIDANDO ... " + index);
                    if(value === "" || value == null){
                        console.log("    ERROR en " + index);
                        $scope.error =  "No has cumplido con la información requerida verifica "+ index +" y los campos restantes";
                        $scope.class = "error";
                        respuesta = false;
                        exit();
                    }
                    else{
                        //console.log("OK");
                        $scope.error =  "";
                        $scope.class = "success";
                        respuesta = true;
                    }
                });
                return respuesta;
            };

            $scope.cancelar = function () {
                $state.go('menu.misReclamos');
            };

        }])

    .controller('nuevoReclamo23Ctrl', ['$scope', '$stateParams', '$http', '$state', '$ionicPlatform',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state, $ionicPlatform) {

            $scope.form1 = $stateParams;

            $scope.form = {
                'campana': "",
                'tienda': "",
                'tipo_asegurado': "",
                'tipo_poliza': "",
                'producto': "1",
                'marca': '',
                'referencia': '',
                'imei': ''
            };

            //traer todos las campanas
            $http({
                url: host + '/campanas/search/' + localStorage.getItem("CRAWFORD_aseguradora"),
                method: "GET"
            }).then(function (result) {
                console.log(result);
                $scope.campanas = result.data.body;
            }, function (err) {
                console.log(err);
            });

            $scope.onChange_campana = function (campana) {
                //traer todos las tiendas
                $http({
                    url: host + '/tiendas/search/' + campana,
                    method: "GET"
                }).then(function (result) {
                    console.log(result);
                    $scope.tiendas = result.data.body;
                }, function (err) {
                    console.log(err);
                });
            };

            $scope.onChange_tienda = function (tienda) {
                //traer todos las tipo_asegurados
                $http({
                    url: host + '/tipo_asegurados/search/' + tienda,
                    method: "GET"
                }).then(function (result) {
                    console.log(result);
                    $scope.asegurados = result.data.body;
                }, function (err) {
                    console.log(err);
                });
            };

            $scope.onChange_tipo_asegurado = function (tipo_asegurado) {
                //traer todos las tipo_poliza
                $http({
                    url: host + '/tipo_polizas/search/' + tipo_asegurado,
                    method: "GET"
                }).then(function (result) {
                    console.log(result);
                    $scope.tipo_polizas = result.data.body;
                }, function (err) {
                    console.log(err);
                });
            };

            $scope.onChange_tipo_poliza = function (tipo_polizas) {
                //traer todos las tipo_poliza
                $http({
                    url: host + '/tipo_polizas/get/' + tipo_polizas,
                    method: "GET"
                }).then(function (result) {
                    //creaate formulario in tipo_polizas
                    console.log(result);
                    $scope.formularios = JSON.parse(result.data.body[0].formulario).pasos;
                    console.log($scope.formularios);
                }, function (err) {
                    console.log(err);
                });
            };

            $scope.go_pass3 = function () {
                try{
                    //consumir respuestas en json formuario backend
                    $scope.form.jsonrespuestaform = '{ "pasos": [';
                    $scope.formularios.forEach(function (element) {
                        $scope.form.jsonrespuestaform += '{ "name" : "' + element.id + '", "value" : "' + document.getElementById(element.id).value + '" }';
                        ;
                        //$scope.form.jsonrespuestaform += element.name;
                    }, this);
                    $scope.form.jsonrespuestaform += " ]}";
                }
                catch(e){
                    console.log('un intento fallido');
                }
                
                //recopilacion datos
                $scope.data_form = {
                    'reclamoid': "NEWRECLAMO",
                    'cedula': $scope.form1.cedula,
                    'nombreasegurado': $scope.form1.nombreasegurado,
                    'apellidoasegurado': $scope.form1.apellidoasegurado,
                    'fechanacimientoasse': $scope.form1.fechanacimientoasse,
                    'ciudadrecidencia': $scope.form1.ciudadrecidencia,
                    'telefonofijo': $scope.form1.telefonofijo,
                    'celular': $scope.form1.celular,
                    'direccion': $scope.form1.direccion,
                    'email': $scope.form1.email,
                    'telefonolaboral': $scope.form1.telefonolaboral,
                    'producto': $scope.form.producto,
                    'marca': $scope.form.marca,
                    'referencia': $scope.form.referencia,
                    'imei': $scope.form.imei,
                    'tipo_poliza': $scope.form.tipo_poliza
                };
                console.log($scope.data_form);
                if (this.validate() !== false) {
                    $state.go('nuevoReclamo33', $scope.data_form);
                }                   
            };

            $scope.validate = function () {    
                var respuesta = false;            
                angular.forEach($scope.form,function(value,index){
                    console.log("VALIDANDO ... " + index);
                    if(value === "" || value == null){
                        console.log("    ERROR en " + index);
                        $scope.error =  "No has cumplido con la información requerida verifica "+ index +" y los campos restantes";
                        $scope.class = "error";
                        respuesta = false;
                        exit();
                    }
                    else{
                        //console.log("OK");
                        $scope.error =  "";
                        $scope.class = "success";
                        respuesta = true;
                    }
                });
                return respuesta;
            };


            $scope.cancelar = function () {
                $state.go('menu.misReclamos');
            };

            $ionicPlatform.registerBackButtonAction(function () {
                alert('no puedes ir atras');
            }, 100);

        }])

    .controller('nuevoReclamo33Ctrl', ['$scope', '$stateParams', '$http', '$state', '$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state, $ionicLoading) {

            console.log($stateParams);
            

            $scope.form1 = $stateParams;

            $scope.data_form = {
                'reclamoid': $scope.form1.reclamoid,
                'cedula': $scope.form1.cedula,
                'nombreasegurado': $scope.form1.nombreasegurado,
                'apellidoasegurado': $scope.form1.apellidoasegurado,
                'fechanacimientoasse': $scope.form1.fechanacimientoasse,
                'ciudadrecidencia': $scope.form1.ciudadrecidencia,
                'telefonofijo': $scope.form1.telefonofijo,
                'celular': $scope.form1.celular,
                'direccion': $scope.form1.direccion,
                'email': $scope.form1.email,
                'telefonolaboral': $scope.form1.telefonolaboral,
                'producto': $scope.form1.producto,
                'referencia': $scope.form1.referencia,
                'marca': $scope.form1.marca,
                'imei': $scope.form1.imei,
                'producto_reclamo': 'APPCRAWFORD',
                'fechasiniestro': "",
                'horasiniestro': "",
                'descripcionsiniestro': "",
                'textobackend': "SIN TEXTO",
                'observaciones': "SIN OBSERVACIONES",
                'motivobaja': "NO BAJA",
                'cliente': localStorage.getItem("CRAWFORD_cliente"),
                'estados_poliza': "3",
                'ciudadsiniestro': "",
                'deptsiniestro': "",
                'plan_polizas': "SEGUROSCRAWFORD" 
            };

            //traer todos las ciudades
            $http({
                url: host + '/ciudades/all',
                method: "GET"
            }).then(function (result) {
                console.log(result);
                $scope.departamentos = result.data.body;
            }, function (err) {
                console.log(err);
            });

            $scope.cargarCiudades = function(){
                id = document.getElementById('departamento').value;
                console.log('id de departamento',id);
                
                $http({
                    url: host + '/ciudades/show_ciudades/'+ id,
                    method: 'GET'
                }).then(function (response){
                    console.log(response);
                    $scope.ciudades = response.data.body;
                    
                }, function (e){
                    console.log(e);
                    
                })
            }

            //enviar datos post a laravel create reclamo
            $scope.generar_caso = function () {
                // Setup the loader
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                console.log($scope.data_form);
                if (this.validate() !== false) {
                    $http({
                        url: host + '/reclamos/create',
                        method: "POST",
                        data: $scope.data_form
                    }).then(function (result) {
                        console.log(result);
                        if (result.data.Status == 'false') {
                            alert(result.data.Message);
                            $ionicLoading.hide();
                        }
                        else {
                            alert("Caso generado # " + result.data.body.reclamoid);
                            $ionicLoading.hide();
                            $state.go('menu.misReclamos', {
                                'CRAWFORD_cliente': localStorage.getItem('CRAWFORD_cliente')
                            });/*
                            $state.go('checklist', {
                                "id_reclamo": result.data.body.id,
                                "tipo_poliza": $scope.form1.tipo_poliza
                            });*/
                        }
                    }, function (err) {
                        console.log(err);
                        $ionicLoading.hide();
                    });  
                } 
            };

            $scope.validate = function () {    
                var respuesta = false; 
                angular.forEach($scope.data_form,function(value,index){
                    console.log("VALIDANDO ... " + index);
                    if(value === "" || value == null){
                        console.log("    ERROR en " + index);
                        $scope.error =  "No has cumplido con la información requerida verifica "+ index +" y los campos restantes";
                        $scope.class = "error";
                        $ionicLoading.hide();
                        respuesta = false;
                        // exit();
                    }
                    else{
                        //console.log("OK");
                        $scope.error =  "";
                        $scope.class = "success";
                        respuesta = true;
                    }
                });
                return respuesta;
            };

            $scope.cancelar = function () {
                $state.go('menu.misReclamos');
            };
        }

    ])

    .controller('checklistCtrl', ['$scope', '$stateParams', '$cordovaDialogs', '$http', '$state', '$ionicLoading', '$rootScope',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $cordovaDialogs, $http, $state, $ionicLoading, $rootScope) {

            $scope.caso = $stateParams;
            console.log('State params cheklistCTRL');
            console.log($scope.caso);
            $scope.contador = {
                'list' : 0,
                'doc' : 0,
                'pivot': 0,
                'habilitado': false
            };

            //intervalos de 3 segundos y refresca los Datos
            this.task = setInterval(function() {
                $scope.refreshData();
            }, 8000);


            $scope.refreshData = function(){                
                //traer los checklist
                    $http({
                        url: host + '/listchekings/search/' + $scope.caso.tipo_poliza + '/' + $scope.caso.id_reclamo,
                        method: "GET"
                    }).then(function (result) {
                        console.log('ALL CHECKLIST FOR Tipo_poliza ');
                        console.log(result);
                        $scope.listas = result.data.body;
                        $scope.contador.pivot = result.data.body.length;
                    }, function (err) {
                        console.log(err);
                    });
                    
                    //buscar toda la informacion de los documentos y la cantidad
                    $http({
                        url: host + '/documentsmodel/verificarcompletos',
                        method: "POST",
                        contentType: "text/xml",
                        dataType: "text",
                        data: {
                            'reclamo' : $scope.caso.id_reclamo
                        }
                    }).then(function (result) {
                        console.log('VERIFICACION DE CUANTOS ESTAN COMPLETOS');
                        console.log(result.data);
                        $scope.documents = result.data.body;
                        console.log('Verificacion');
                        console.log($scope.contador.pivot +" >="+ result.data.body.length);
                        if($scope.contador.pivot == result.data.body.length){
                            $scope.contador.habilitado = true;
                        }
                        else{
                            $scope.contador.habilitado = false;
                        }
                    }, function (err) {
                        console.log(err);
                    });
            };

            console.log($scope.caso);

            //traer los checklist
            $http({
                url: host + '/listchekings/search/' + $scope.caso.tipo_poliza,
                method: "GET"
            }).then(function (result) {
                console.log(result);
                $scope.listas = result.data.body;
            }, function (err) {
                console.log(err);
            });

            //buscar toda la informacion de los documentos y la cantidad
            $http({
                url: host + '/documentsmodel/verificarcompletos',
                method: "POST",
                contentType: "text/xml",
                dataType: "text",
                data: {
                    'reclamo' : $scope.caso.id_reclamo
                }
            }).then(function (result) {
                console.log('resultado....');
                console.log(result.data);
                $scope.documents = result.data.body;
            }, function (err) {
                console.log(err);
            });

            $scope.aviso = function (id_reclamo, tipo_poliza, ayuda) {
                alert(ayuda);
                ///$state.go('menu.caso1402', {
                ///    'id_reclamo': id_reclamo,
                ///    'tipo_poliza': tipo_poliza,
                ///    'ayuda': ayuda
               /// });
            }

            $scope.go_record = function(id_reclamo, id_checklist, tipo_poliza, ayuda){
                alert(ayuda);
                //start Recording
                window.plugins.audioRecorderAPI.record(function(msg) {
                    // complete
                    console.log('ok: ' + msg);
                    $rootScope.audioFilePath = msg;
                }, function(msg) {
                    // failed
                    aleconsole.logrt('ko: ' + msg);
                }, 30); // record 30 seconds
    
                $state.go('record', {
                    'id_reclamo': id_reclamo,
                    'id_checklist': id_checklist,
                    'tipo_poliza': tipo_poliza
                });
            };

            $scope.reclamar = function(){
                // Setup the loader
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });
                $http({
                    url: host + '/reclamos/updateToEnEstudio',
                    method: "POST",
                    contentType: "text/xml",
                    dataType: "text",
                    data: {
                        'reclamo' : $scope.caso.id_reclamo
                    }
                }).then(function (result) {
                    console.log('resultado....');
                    console.log(result.data);
                    $scope.documents = result.data.body;
                    $ionicLoading.hide();
                    $state.go('menu.misReclamos', {
                        CRAWFORD_cliente: localStorage.getItem('CRAWFORD_cliente')
                    });
                }, function (err) {
                    console.log(err);
                    $ionicLoading.hide();
                });
            };

            $scope.reclamarno = function(){
                alert('debes completar todos los documentos');
            };

            $scope.go_scanner = function (id_reclamo, id_checklist, tipo_poliza, ayuda) {
                alert(ayuda);
                $state.go('escanear', {
                    'id_reclamo': id_reclamo,
                    'id_checklist': id_checklist,
                    'tipo_poliza': tipo_poliza
                });
            };

            /*$scope.go_record = function (id_reclamo, id_checklist, tipo_poliza, ayuda) {
                alert(ayuda);
                $state.go('grabarAudio', {
                    'id_reclamo': id_reclamo,
                    'id_checklist': id_checklist,
                    'tipo_poliza': tipo_poliza
                });
            };*/

        }

        


    ])

    .controller('escanearCtrl', ['$scope', '$stateParams', '$cordovaCamera', '$cordovaActionSheet', '$http', '$state', '$ionicPlatform', '$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName

        function ($scope, $stateParams, $cordovaCamera, $cordovaActionSheet, $http, $state, $ionicPlatform, $ionicLoading) {

            $scope.reclamo = $stateParams;
            $scope.file = {
                'file': '',
                'host': host
            };
            console.log($scope.reclamo);
            $scope.respuesta = {
                'ms': 'Respuesta',
                'foto': ""
            }

            //buscar toda la informacion del reclamo
            $http({
                url: host + '/documentsmodel/reclamo/' + $scope.reclamo.id_reclamo + '?listcheking=' + $scope.reclamo.id_checklist ,
                method: "GET"
            }).then(function (result) {
                console.log('resultado');
                console.log(result.data.body);
                $scope.documents = result.data.body;
            }, function (err) {
                console.log(err);
            });

            $scope.delete = function (document) {
                var r = confirm("Se eliminara esta foto. Esta de acuerdo?");
                if (r == true) {
                    // Setup the loader
                    $ionicLoading.show({
                        content: 'Loading',
                        animation: 'fade-in',
                        showBackdrop: true,
                        maxWidth: 200,
                        showDelay: 0
                    });
                    $http({
                        url: host + '/documentsmodel/delete/' + document,
                        method: "GET"
                    }).then(function (result) {
                        console.log(result);
                        alert(result.data.Message);
                        //$state.go('escanear', $scope.reclamo);
                        $ionicLoading.hide();
                        $state.transitionTo('escanear', $scope.reclamo, {reload: true, notify:true});
                    }, function (err) {
                        console.log(err);
                        $ionicLoading.hide();
                    });
                }                
            };

            $scope.folderFile = function(){
                var cameraOptions = {
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    destinationType: Camera.DestinationType.DATA_URL,      
                    quality: 75,
                    targetWidth: 300,
                    targetHeight: 300,
                    encodingType: Camera.EncodingType.JPEG,      
                    correctOrientation: true
                }; 
                $cordovaCamera.getPicture(cameraOptions).then(function (imageData) {
                    $scope.imgURI = "data:image/jpeg;base64," + imageData;
                    $scope.respuesta.foto = "data:image/jpeg;base64," + imageData;
                    var r = confirm("Se enviará esta foto. Esta de acuerdo?");
                    if (r == true) {
                        $scope.saveimage();
                    }  
                }, function (err) {
                    console.log(err);
                });
            };

            $scope.takePicture = function () {
                var options = {
                    quality: 75,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 300,
                    targetHeight: 300,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false

                };


                $cordovaCamera.getPicture(options).then(function (imageData) {
                    $scope.imgURI = "data:image/jpeg;base64," + imageData;
                    //$scope.saveimage($scope.imgURI);
                    $scope.respuesta.foto = "data:image/jpeg;base64," + imageData;
                    var r = confirm("Se enviará esta foto. Esta de acuerdo?");
                    if (r == true) {
                        $scope.saveimage();
                    }  
                }, function (err) {
                    console.log(err);
                });

            };

            $scope.data = {
                'file': ''
            };

            $scope.send = function (scope, element, attributes) {
                // element.bind("change", function (changeEvent) {
                //     var reader = new FileReader();
                //     reader.onload = function (loadEvent) {
                //         scope.$apply(function () {
                //             scope.fileread = loadEvent.target.result;
                //         });
                //     }
                //     reader.readAsDataURL(changeEvent.target.files[0]);
                // });
                var r = confirm("Se enviará esta foto. Esta de acuerdo?");
                if (r == true) {
                    $scope.saveimage();
                }                
            };

            $scope.saveimage = function () {
                //var file = document.getElementById('sortpicture').value;
                //var file_data = file.prop('files')[0];
                //$scope.respuesta.ms = file_data;
                //var file_data = file;
                /*var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('reclamo', $scope.reclamo.id_reclamo);
                form_data.append('cliente', localStorage.getItem("CRAWFORD_cliente"));
                form_data.append('listcheking', $scope.reclamo.listcheking);
                form_data.append('FileType', 'camara');
                form_data.append('redirect', 'crawford');*/

                //
                // var data = {
                //     'file': $scope.data.file,
                //     'reclamo': $scope.reclamo.id_reclamo,
                //     'cliente': localStorage.getItem("CRAWFORD_cliente"),
                //     'listcheking': $scope.reclamo.listcheking,
                //     'FileType': 'camara',
                //     'redirect': 'crawford'
                // };
                var data = {
                    'FileType': 'camara',
                    'image_content': $scope.respuesta.foto,
                    'reclamo': $scope.reclamo.id_reclamo,
                    'cliente': localStorage.getItem("CRAWFORD_cliente"),
                    'listcheking': $scope.reclamo.id_checklist
                };

                // Setup the loader
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                $http({
                    url: host + '/documentsmodel/uploadSubmitAPP',
                    method: "POST",
                    data: data
                }).then(function (result) {
                    if (result.data.Status == 'false') {
                        $ionicLoading.hide();
                        alert(result.data.Message);
                    }
                    else {
                        console.log(result);
                        $scope.respuesta.ms = result;
                        $scope.respuesta.foto = data;
                        $ionicLoading.hide();
                        $state.transitionTo('escanear', $scope.reclamo, {reload: true, notify:true});
                    }
                }, function (err) {
                    console.log(err);
                    $ionicLoading.hide();
                });

            };

            $scope.go_checklist = function () {
                $state.transitionTo('checklist', {
                        "id_reclamo": $stateParams.id_reclamo,
                        "tipo_poliza": $stateParams.tipo_poliza
                    }, {reload: true, notify:true});
               /* $state.go('checklist', {
                    "id_reclamo": $stateParams.id_reclamo,
                    "tipo_poliza": $stateParams.tipo_poliza
                });*/
            };

            $scope.borrar = function () {
                var options = {
                    title: '¿Quieres borrar esta imagen?',
                    buttonLabels: ['Repetir'],
                    addCancelButtonWithLabel: 'Cancelar',
                    androidEnableCancelButton: true,
                    winphoneEnableCancelButton: true,
                    addDestructiveButtonWithLabel: 'Borrarla'

                };

                $cordovaActionSheet.show(options).then(function (btnIndex) {
                    var index = btnIndex;

                }, false);

            };

            $ionicPlatform.registerBackButtonAction(function () {
                console.log('no se puede ir atras');
            }, 100);

        }

    ])

    .controller('grabarAudioCtrl', ['$scope', '$stateParams', '$cordovaCamera', '$cordovaActionSheet', '$http', '$state', '$ionicPlatform', '$ionicLoading', '$cordovaMedia',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $cordovaCamera, $cordovaActionSheet, $http, $state, $ionicPlatform, $ionicLoading, $cordovaMedia) {

            //no funciona para nada

        }])

    .controller('caso1402Ctrl', ['$scope', '$stateParams', '$http', '$state', '$ionicPlatform', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state, $ionicPlatform) {

            console.log($stateParams);
            $scope.data_pass = $stateParams;

            //buscar toda la informacion del reclamo
            $http({
                url: host + '/reclamos/get/' + $stateParams.id_reclamo,
                method: "GET"
            }).then(function (result) {
                console.log(result);
                $scope.reclamos = result.data.body[0];
            }, function (err) {
                console.log(err);
            });

            //buscar toda la informacion del reclamo
            $http({
                url: host + '/notificaciones/reclamo/' + $stateParams.id_reclamo,
                //url: host+'/notificaciones/get/1',
                method: "GET"
            }).then(function (result) {
                console.log(result);
                $scope.notificaciones = result.data.body;
            }, function (err) {
                console.log(err);
            });

            $ionicPlatform.registerBackButtonAction(function () {
                $state.go('menu.misReclamos', {
                    "CRAWFORD_cliente": localStorage.getItem("CRAWFORD_cliente")
                });
            }, 100);

        }])

    .controller('preguntasFrecuentesCtrl', ['$scope', '$stateParams', '$http', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state) {

        }])

    .controller('enviarMensajeCtrl', ['$scope', '$stateParams', '$http', '$state', '$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state, $ionicLoading) {

            $scope.sendmessage = {
                'mensaje': '',
                'telefono': '1',
                'franja': '1',
                'diasemana': '1',
                'estado': 'ENVIADO',
                'cliente': localStorage.getItem("CRAWFORD_cliente")
            };

            $scope.send = function () {
                // Setup the loader
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                console.log($scope.sendmessage);
                if (this.validate() !== false) {
                    $http({
                        url: host + '/solicitudes/create',
                        method: "POST",
                        contentType: "text/xml",
                        dataType: "text",
                        data: $scope.sendmessage,
                    }).then(function (result) {
                        console.log(result);
                        if (result.data.Status == 'false') {
                            alert(result.data.Message);
                            $ionicLoading.hide();
                        }
                        else {
                            $scope.sendmessage.mensaje = '';
                            $ionicLoading.hide();
                            $state.go('menu.ayuda');
                        }
                    }, function (err) {
                        console.log(err);
                        $ionicLoading.hide();
                    });
                }
            };


            $scope.validate = function () {    
                var respuesta = false;            
                angular.forEach($scope.sendmessage,function(value,index){
                    console.log("VALIDANDO ... " + index);
                    if(value === "" || value == null){
                        console.log("    ERROR en " + index);
                        $scope.error =  "No has cumplido con la información requerida verifica "+ index +" y los campos restantes";
                        $scope.class = "error";
                        $ionicLoading.hide();
                        respuesta = false;
                        exit();
                    }
                    else{
                        //console.log("OK");
                        $scope.error =  "";
                        $scope.class = "success";
                        respuesta = true;
                    }
                });
                return respuesta;
            };



        }])

    .controller('quieroQuEmeLlamenCtrl', ['$scope', '$stateParams', '$http', '$state', '$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state, $ionicLoading) {

            $scope.sendmessage = {
                'mensaje': '1',
                'telefono': localStorage.getItem("CRAWFORD_celular"),
                'franja': '',
                'diasemana': '',
                'estado': 'ENVIADO',
                'cliente': localStorage.getItem("CRAWFORD_cliente")
            };

            $scope.send = function () {
                // Setup the loader
                $ionicLoading.show({
                    content: 'Loading',
                    animation: 'fade-in',
                    showBackdrop: true,
                    maxWidth: 200,
                    showDelay: 0
                });

                console.log($scope.sendmessage);
                if (this.validate() !== false) {
                    $http({
                        url: host + '/solicitudes/create',
                        method: "POST",
                        contentType: "text/xml",
                        dataType: "text",
                        data: $scope.sendmessage,
                    }).then(function (result) {
                        console.log(result);
                        if (result.data.Status == 'false') {
                            alert(result.data.Message);
                            $ionicLoading.hide();
                        }
                        else {
                            $scope.sendmessage.telefono = '';
                            $scope.sendmessage.diasemana = '';
                            $scope.sendmessage.franja = '';
                            $ionicLoading.hide();
                            $state.go('menu.ayuda');
                        }
                    }, function (err) {
                        console.log(err);
                        $ionicLoading.hide();
                    });
                }
            };


            $scope.validate = function () {    
                var respuesta = false;            
                angular.forEach($scope.sendmessage,function(value,index){
                    console.log("VALIDANDO ... " + index);
                    if(value === "" || value == null){
                        console.log("    ERROR en " + index);
                        $scope.error =  "No has cumplido con la información requerida verifica "+ index +" y los campos restantes";
                        $scope.class = "error";
                        $ionicLoading.hide();
                        respuesta = false;
                        exit();
                    }
                    else{
                        //console.log("OK");
                        $scope.error =  "";
                        $scope.class = "success";
                        respuesta = true;
                    }
                });
                return respuesta;
            };

        }])

    .controller('chatCtrl', ['$scope', '$stateParams', '$http', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state) {

            var fila = 0;
            $scope.historiales = [];

            $scope.message = {
                'message': '',
                'state': 'ENVIADO',
                'sender': localStorage.getItem("CRAWFORD_cliente"),
                'receptor': '1' //id administrador
            };

            //buscar toda la informacion del chaat
            setInterval(function () {
                $scope.show_history();
            }, 8000);

            $scope.addmessage = function () {
                console.log($scope.message);
                if (this.validate() !== false) {
                    $http({
                        url: host + '/chats/create',
                        method: "POST",
                        contentType: "text/xml",
                        dataType: "text",
                        data: $scope.message,
                    }).then(function (result) {
                        console.log(result);
                        if (result.data.Status == 'false') {
                            alert(result.data.Message);
                        }
                        else {
                            $scope.message.message = '';
                            $scope.show_history();
                        }
                    }, function (err) {
                        console.log(err);
                    });
                }
            };

            $http({
                url: host + '/chats/historial/' + $scope.message.sender + '/1',
                method: "GET"
            }).then(function (result) {
                console.log(result);
                $scope.historiales = result.data.body;
                fila = result.data.body.length;
            }, function (err) {
                console.log(err);
            });

            $scope.show_history = function(){
                $http({
                    url: host + '/chats/historial/' + $scope.message.sender + '/1',
                    method: "GET"
                }).then(function (result) {
                    filaother = fila;
                    for(i = result.data.body.length; i > filaother ; i--){
                        fila = result.data.body.length;
                        console.log('filass__'+i);
                        $scope.historiales.push(result.data.body[i-1]);
                        console.log(result.data.body[i-1]);
                    }
                    console.log('fila # '+fila);
                }, function (err) {
                    console.log(err);
                });
            };

            $scope.validate = function () {
                if ($scope.message.message === '') {
                    console.log('Mensaje no enviado, se encuentra vacio');
                    return false;
                }
            };


        }])

    .controller('misPolizasCtrl', ['$scope', '$stateParams', '$http', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state) {

        }])

    .controller('pLizaPantallaProtegidaCtrl', ['$scope', '$stateParams', '$http', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state) {

        }])

    .controller('pAGOPAYUCtrl', ['$scope', '$stateParams', '$http', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams, $http, $state) {

        }])
    .controller('recordCtrl', ['$scope', '$stateParams', '$cordovaCamera', '$cordovaActionSheet', '$http', '$state', '$ionicPlatform', '$ionicLoading', '$cordovaMedia', '$ionicHistory', '$rootScope', '$timeout',
        function ($scope, $stateParams, $cordovaCamera, $cordovaActionSheet, $http, $state, $ionicPlatform, $ionicLoading, $cordovaMedia, $ionicHistory, $rootScope, $timeout) {

            $scope.reclamo = $stateParams;
            $scope.is_recording = true;
            $scope.is_playing = false;
            var my_media;
            $scope.file = {
                'file': '',
                'host': host
            };
            console.log('-----STATE PARAMETROS RECLAMO RECORD AUDIO');
            console.log($scope.reclamo);

            $scope.respuesta = {
                'ms': 'Respuesta',
                'audio': ""
            };
            $scope.playRecording = function(){
                //play Recording
                my_media = new Media($rootScope.audioFilePath, function(success) {
                }, function (err) {
                    console.log(err);
                });

                // Get duration
                var counter = 0;
                var dur = 0;
                var timerDur = setInterval(function() {
                    counter = counter + 100;
                    if (counter > 2000) {
                        clearInterval(timerDur);
                        timerDur = null;
                    }
                    dur = my_media.getDuration();
                    if (dur > 0) {
                        clearInterval(timerDur);
                        timerDur = null;
                    }
                }, 100);

                // Update media position every second
                var mediaTimer = setInterval(function () {
                    // get media position
                    my_media.getCurrentPosition(
                        // success callback
                        function (position) {
                            if (position > 0) {
                                console.log((position) + " sec");
                            }else {
                                $scope.is_playing = false;
                                $timeout(function() {
                                    $scope.$apply();
                                }, 0);
                                clearInterval(mediaTimer);
                                mediaTimer = null;
                            }
                        },
                        // error callback
                        function (e) {
                            console.log("Error getting pos=" + e);
                        }
                    );
                }, 3000);


                $scope.is_playing = true;
                my_media.play();
            };
            $scope.stopRecording = function(){
                //stop Recording
                if ($scope.is_recording) {
                    $scope.is_recording = false;
                    window.plugins.audioRecorderAPI.stop(function(msg) {
                        // success
                        $rootScope.audioFilePath = msg;
                        alert('ok: ' + msg);
                    }, function(msg) {
                        // failed
                        alert('ko: ' + msg);
                    });
                }else {
                    $scope.is_playing = false;
                    my_media.stop();
                    my_media.release();
                }

            };
            $scope.cancelAudio = function(){
                //cancel Recording
                $ionicHistory.goBack();
            };
            $scope.saveAudio = function(){
                //save Audio into laravel integration

                var win = function (r) {
                    console.log("Code = " + r.responseCode);
                    console.log("Response = " + r.response);
                    console.log("Sent = " + r.bytesSent);
                    alert('Audio guardado satisfactoriamente!');
                    $ionicHistory.goBack();
                };

                var fail = function (error) {
                    alert("Ocurrio un error: Code = " + error.code);
                    console.log("upload error source " + error.source);
                    console.log("upload error target " + error.target);
                    $ionicHistory.goBack();
                };

                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.fileName = $rootScope.audioFilePath.substr($rootScope.audioFilePath.lastIndexOf('/') + 1);
                options.mimeType = "audio/m4a";

                var ft = new FileTransfer();
                ft.onprogress = function(progressEvent) {
                    if (progressEvent.lengthComputable) {
                        console.log(progressEvent.loaded / progressEvent.total);
                        // loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
                    } else {
                        // loadingStatus.increment();
                    }
                };
                ft.upload($rootScope.audioFilePath, encodeURI(host + '/test/uploadaudio?id_checklist='+$scope.reclamo.id_checklist+'&id_reclamo='+$scope.reclamo.id_reclamo+'&tipo_poliza='+$scope.reclamo.id_reclamo+'&id_user='+localStorage.getItem('CRAWFORD_cliente')), win, fail, options);
            };

            $ionicPlatform.registerBackButtonAction(function () {
                console.log('no se puede ir atras');
            }, 100);

        }])

